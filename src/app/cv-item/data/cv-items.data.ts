import * as EN from './cv-items.data.en';
import * as IT from './cv-items.data.it';

export const DATA = {
  'EN': EN,
  'IT': IT,
};
