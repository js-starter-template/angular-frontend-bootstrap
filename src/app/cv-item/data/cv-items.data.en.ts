import { CvItem } from '../cv-item';

/* eslint-disable max-len */
export const GENERAL = {
  name: 'Angelo Santarella',
  position: 'Software Engineer',
  description: `Angelo is a DevOps Engineer and a Backend developer with experiences in NodeJs, Typescript, MySQL, MSSQL such as developer as analyst.
  Angelo has been graduated from the University of Salerno in Computer Science in September 2011 .
  Angelo is currently work as software engineer at ACCA software S.p.A., developing DevOps, Backend, Full stack web applications and desktop software solutions.
  Previously, Angelo have worked in counseling at AleniaAermacchi S.p.A. (now Leonardo) as IT Systems Integrator.
  His professional experiences started in July 2011 with the ItalSystem Srl developing the analysis for two research projects, then in February 2012 he begin the consultancy for AleniaAermacchi.
  
  His daily commitments, and his work experience, skill he about DevOps (using Docker and Kubernetes), backend and Full stack web solutions, and searching new and reliable technologies about these environments.
  His university background skill me about Android application also, since for his bachelor degree thesis he worked on '"Environment State Virtual Sensor for Android"​ an open source library.
  

Angelo is looking for new opportunities which include modern, progressive technologies in a collaborative working environment. Learning new things and trying out new stuff are part of his motivation. New roles are of interest if he can accompany the software end to end - from planning, implementation, deployment towards monitoring. 
Amplified feedback loops and a culture of continuous learning and experimentation define a proper working environment for him. 
Job offers outside of southern Italy or "Full Remote" are not relevant to him.

This website is designed to be a simple, self-hosted online-CV, based on Angular. To find out more about the project, please click on the AngularCV logo in the bottom right corner.`,
};
/* eslint-enable max-len */

export const CVITEMS: CvItem[] = [
  {
    title: 'ACCA software S.p.A.',
    subtitle: 'Backend Developer e DevOps Engineer',
    begin: '2014-03',
    end: '',
    description: `DevOps (2019-today): Internal GitLab service management and introduction, CI / 
    CD pipeline development via GitLab, Kubernetes cluster management, Docker 
    internal server management, AWS suite management: EKS, Fargate, ECR, S3, EC2, 
    CloudWatch, XRay, SES, IAM, VPC, VPN. Management, development and porting, 
    from legacy systems to the K8s cluster. Management of third-party applications on 
    K8s clusters: Ingress, CertManager, Prometheus, Keycloak. EC2 windows 
    management for legacy systems.
    Page 2/3 - Curriculum vitae of 
    Santarella Angelo
    • Web Services (2015-present): Maintenance and development of: subscription 
    authorization service, account authentication service, ecommerce service, 
    automatic subscription renewal service, price list service, sales catalog 
    administration, Dashboard service (Angular / NodeJs) for commercial operators 
    aimed at customer analysis, performance, sales and statistics. DBAdmin for the 
    aforementioned services. Servers developed using GitLab, K8s, Docker, NodeJS, 
    MySQL.
    • ACCA Catalogue (2014-2017): Server: Definition and Development of Apache-PHP-MySQL web application, using Symfony, Bootstrap, JQuery. ElasticSearch search 
    engine Integration. AWS EC2 machines Architecture based. Client: Definition and 
    development of the module that can be integrated into all the company's software 
    solutions for interaction with the ACCA online catalogue`,
    tags: ['Javascript', 'Angular', 'NodeJS', 'MySql', 'MSSql', 'Kubernetes', 'Git', 'Docker', 'Gitlab',
      'Scrum', 'AWS', 'PHP', 'elasticsearch', 'bash'],
    link: 'https://www.accasoftware.com',
    thumbnail: 'LogoACCA_150px.png',
  },
  {
    title: 'Alenia Aermacchi - Leonardo S.p.A.',
    subtitle: 'System Integrator & Software Analyst/Developer',
    begin: '2012-02',
    end: '2014-03',
    description: ` MPDS (Mission Planning and Debriefing System)
    Planning and debriefing system integration with the M346 aircraft avionics systems for ITAF, 
    RSAF, IAF customers. Debugging and functional tests.
    • Debriefing System Management (client and suppliers coordination), BFEs (Buyer Furnished 
    Equipment) analysis and development of cockpit reconstruction from avionic messages (C + +).
    • Software installation and flight test, Cazaux (FRA), for the customer RSAF (Republic of 
    Singapore Air Force) training system for pilots ITAF, RSAF, IAF
    • Software tools design and development, used for decision and management of the training 
    syllabus for M346 pilots. Development of a C# software solution supported by Access database.
    `,
    tags: ['C#', 'sistem integration', 'SQL', 'Office'],
    link: 'https://www.leonardocompany.com/en/air',
    thumbnail: 'leonardo-300x300.jpg',
  },
  {
    title: ' ItalSystem s.r.l.',
    subtitle: 'Software Analyst',
    begin: '2011-07',
    end: '2012-02',
    description: `M346 OS Flight Control Computer Project – AleniaAermacchi SpA - V&V Area. 
    Test procedures definition for the Unit Testing activity drafted in compliance to the standard DO178-B 
    Level A in language Ada95 for the planned verification of structural coverage, functional and MCDC.
    Utilities Design and development for Change Impact Analysis across several UT Build procedures. 
    Activities implemented through Visual Studio 2010 and VB .NET.`,
    tags: ['VB.net', 'BPEL', 'Quality Assurance', 'SOA', 'Testrail', 'Subversion'],
    link: 'http://www.italsystemsrl.it/',
    thumbnail: 'italsystem.jpg',
  }
];

export const CERTIFICATES: CvItem[] = [
  {
    title: 'AWS Techshift',
    subtitle: 'Amazon Web Services (AWS)',
    begin: '',
    end: '2021-04-14',
    description: '',
    link: '',
    thumbnail: '',
    attachment: '',
  },
  {
    title: 'Trinity College Certification for the English language',
    subtitle: 'LTS Language & Testing Service',
    begin: '',
    end: '2006-05-31',
    description: '',
    thumbnail: '',
    attachment: '',
  },
];

export const EDUCATION: CvItem[] = [
  {
    title: 'Università degli studi di Salerno',
    subtitle: 'Bachelor of Science | Computer Science',
    begin: '2006-10',
    end: '2011-09',
    description: `Thesis: Environment State Virtual Sensor for Android
    Design and development of the sensor “Environment State Virtual Sensor” an Android library that
    allows the interrogation of environmental scenarios in which an Android device can be. The library has
    been published under LGPL license on Sourceforge. A demonstrator was implemented and it’s called
    "TelephonyManager" that allows to modify phone modes according to environment state. It’s available
    on Play Store and Sourceforge.`,
    tags: ['Android', 'Subversion', 'Java', 'C', 'Assembler', 'eclipse'],
    link: 'https://www.unisa.it/',
    thumbnail: 'logo_unisa.png',
  },
];

export const LANGUAGES = [
  // RATE YOURSELF  =>  100% = NATIVE;  80-99% = FLUENT;  60-79% = ADVANCED;  40-59% = INTERMEDIATE;  20-39% = ELEMENTARY;  0-19% = BEGINNER
  { title: 'English', level: '80' },
  { title: 'Italian | Italiano', level: '100' },
  { title: 'French | Français', level: '10' },
];

export const PROJECTS: CvItem[] = [
  {
    title: 'NodeJS Backend Bootstrap',
    subtitle: 'Backend bootstrap using best technologies and frameworks for NodeJS like: Express, NestJs, Prisma',
    begin: '2021-11',
    end: '',
    description: 'This template is aimed to facilitate and speed up developers start project or, at least, study the frameworks and how to use them',
    tags: ['NodeJs', 'Graphql', 'Prisma', 'TypeScript', 'VisualStudio Code', 'GitLab', 'REST', 'NestJs'],
    link: 'https://gitlab.com/angelosantarella/nodejs-backend-bootstrap',
    thumbnail: 'nodejs.jpg',
  },
  {
    title: 'AngularCV',
    subtitle: 'A simple self-hosted online-CV',
    begin: '2021-11',
    end: '',
    description: 'This project was created for the purpose of having a basic online-CV, which anyone can host by themselves. ' +
      'This very website is the result of it.',
    tags: ['Angular', 'MaterialDesign', 'Apollo', 'CSS', 'TypeScript', 'VisualStudio Code', 'GitLab', 'jsPDF', 'npm'],
    link: 'https://gitlab.com/angelosantarella/angelosantarella.gitlab.io',
    thumbnail: '../AngularCV.svg',
  },
];

export const CONTACT = {
  city: 'Montella, Italy',
  phone: '',
  mail: 'angelo.santarella@gmail.com',
  skype: '', // just the account name
  linkedin: 'https://www.linkedin.com/in/angelosantarella/', // full url
  xing: '', // full url
  github: 'https://github.com/duky2003', // full url
  gitlab: 'https://gitlab.com/angelosantarella', // full url
  stackoverflow: '', // full url
  twitter: 'https://twitter.com/angelosantarella', // full url
  facebook: 'https://www.facebook.com/angelo.santarella/', // full url
  instagram: 'https://www.instagram.com/angelosantarella/', // full url
  other: [
    { title: 'GitLab Page', icon: 'icon-gitlab', link: 'https://angelosantarella.gitlab.io' },
    { title: 'YouTube', icon: 'icon-youtube', link: 'https://www.youtube.com/c/AngeloSantarella' },
  ],
};

export const INTERESTS = [
  {
    title: 'Cycling',
    icon: 'directions_bike',
  },
  {
    title: 'Open Source Software',
    icon: 'code',
  },
  {
    title: 'Drones',
    icon: 'flight',
  },
  {
    title: 'Drone video',
    icon: '4k',
  },
  {
    title: 'Photography',
    icon: 'camera_alt',
  },
  {
    title: '3d Printing',
    icon: 'view_in_ar',
  },
];

