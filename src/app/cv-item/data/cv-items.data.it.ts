import { CvItem } from '../cv-item';

/* eslint-disable max-len */
export const GENERAL = {
  name: 'Angelo Santarella',
  position: 'Software Engineer',
  description: `Angelo è un DevOps Engineer e uno sviluppatore di backend con esperienze in NodeJs, Typescript, MySQL, MSSQL sia come sviluppatore che come analista.
  Angelo si è laureato in Informatica presso l'Università degli Studi di Salerno nel settembre 2011.
  Angelo attualmente lavora come software engineer presso ACCA software S.p.A., sviluppando DevOps, Backend, applicazioni web full stack e soluzioni software desktop.
  In precedenza, Angelo ha lavorato nella consulenza presso AleniaAermacchi S.p.A. (ora Leonardo) come System-Integrator.
  Le sue esperienze professionali iniziano nel luglio 2011 con la ItalSystem Srl sviluppando l'analisi per due progetti di ricerca, poi nel febbraio 2012 inizia la consulenza per AleniaAermacchi.
  
  I suoi impegni quotidiani e la sua esperienza lavorativa, lo specializzano su DevOps (utilizzando Docker e Kubernetes), soluzioni NodeJS backend e full stack e la ricerca di tecnologie nuove e affidabili su questi ambienti.
  Il suo background universitario gli ha fatto conoscere anche le applicazioni Android, visto che per la sua tesi di laurea ha lavorato sull'"Sensore virtuale di stati ambientali per Android"​ una libreria open source.
  

Angelo è alla ricerca di nuove opportunità che includano tecnologie moderne e all'avanguardia in un ambiente di lavoro collaborativo. Imparare cose nuove e provare cose nuove fa parte della sua motivazione. 
Nuovi ruoli sono interessanti se può accompagnare il software end-to-end - dalla pianificazione, implementazione, distribuzione fino al monitoraggio.
Cicli di feedback amplificati e una cultura dell'apprendimento continuo e della sperimentazione definiscono per lui un ambiente di lavoro adeguato.
Le offerte di lavoro al di fuori del sud Italia o "Full Remote" non sono rilevanti per lui.

Questo sito Web è progettato per essere un semplice CV online ospitato autonomamente, basato su Angular. Per saperne di più sul progetto, fare clic sul logo AngularCV nell'angolo in basso a destra.`,
};
/* eslint-enable max-len */

export const CVITEMS: CvItem[] = [
  {
    title: 'ACCA software S.p.A.',
    subtitle: 'Backend Developer e DevOps Engineer',
    begin: '2014-03',
    end: '',
    description: `DevOps (2019-oggi): Gestione e introduzione servizio GitLab interno, sviluppo 
    pipelines CI/CD tramite GitLab, gestione cluster Kubernetes, gestione servers 
    interni Docker, gestione suite AWS: EKS, Fargate, ECR, S3, EC2, CloudWatch, XRay, 
    SES, IAM, VPC, VPN. Gestione, sviluppo e porting, da sistemi legacy, di servizi 
    stateless sul cluster K8s. Gestione applicazioni di terze parti su cluster K8s: Ingress, 
    CertManager, Prometheus, Keycloak. Gestione EC2 windows per sistemi legacy.

    • Servizi Web (2015-oggi): Manutenzione e sviluppo di: servizio di autorizzazione 
    degli abbonamenti, servizio di autenticazione account, servizio ecommerce, servizio 
    rinnovo automatico abbonamenti, servizio listino prezzi, amministrazione catalogo 
    vendita, servizio Dashboard (Angular/NodeJs) per operatori commerciali finalizzato 
    all’analisi cliente, prestazioni, vendita e statistica. DBAdmin per i servizi sopracitati. 
    Server sviluppati utilizzando GitLab, K8s, Docker, NodeJS, NestJs, MySQL, MS SQL.

    • Catalogo ACCA (2014-2017): Server: Definizione e Sviluppo web application 
    Apache-PHP-MySQL, usando Symfony, Bootstrap, JQuery. Integrazione del motore di 
    ricerca ElasticSearch. Architettura basata su macchine AWS EC2. Client: Definizione 
    e Sviluppo del modulo integrabile in tutte le soluzioni software dell’azienda per 
    interazione con il catalogo online ACCA`,
    tags: ['Javascript', 'Angular', 'NodeJS', 'MySql', 'MSSql', 'Kubernetes', 'Git', 'Docker', 'Gitlab',
      'Scrum', 'AWS', 'PHP', 'elasticsearch', 'bash'],
    link: 'https://www.accasoftware.com',
    thumbnail: 'LogoACCA_150px.png',
  },
  {
    title: 'Alenia Aermacchi - Leonardo S.p.A.',
    subtitle: 'System Integrator & Software Analyst/Developer',
    begin: '2012-02',
    end: '2014-03',
    description: `M346 MPDS (Mission Planning and Debriefing System)
    ▪ Integrazione del sistema di pianificazione e debriefing con i sistemi avionici 
    del velivolo M346 per i clienti ITAF, RSAF, IAF. Debugging, Testing 
    funzionali e sviluppo di tool per supporto al testing e a prove volo.
    ▪ Gestione della parte Debriefing del sistema (coordinazione cliente e 
    fornitori), analisi e sviluppo di alcuni BFE (Buyer Furnished Equipment)
    per la funzione di cockpit reconstruction da messaggi avionici (sviluppato 
    in C++).
    ▪ Gestione installazione e manutenzione del software per il cliente RSAF in
    Cazaux (FRA), training del sistema per i piloti ITAF, RSAF, IAF e supporto 
    prove volo in Venegono.
    • Sviluppo di strumenti software per la decisione e la gestione del sillabo di 
    addestramento.
    ▪ Analisi delle procedure generative del training (TNA) piloti sul velivolo 
    M346. Sviluppo di una soluzione software in C#, supportata da database 
    Access`,
    tags: ['C#', 'sistem integration', 'SQL', 'Office'],
    link: 'https://www.leonardocompany.com/en/air',
    thumbnail: 'leonardo-300x300.jpg',
  },
  {
    title: ' ItalSystem s.r.l.',
    subtitle: 'Software Analyst',
    begin: '2011-07',
    end: '2012-02',
    description: `Progetto Flight Control Computer M346 – AleniaSIA S.p.A – Area V&V. 
    Definizione procedure di test per l’attività di Unit Testing redatte in conformità allo 
    standard DO178-B Level A in linguaggio ADA95 per la verifica della copertura 
    strutturale, funzionale ed MCDC prevista. Attività implementata attraverso l’utilizzo 
    del tool VectorCast su compilatore ADAMULTI per PPC.
    Design e sviluppo di utility per la Change Impact Analisys tra diverse Build delle
    procedure di UT. Attività implementata in VB .NET.`,
    tags: ['VB.net', 'BPEL', 'Quality Assurance', 'SOA', 'Testrail', 'Subversion'],
    link: 'http://www.italsystemsrl.it/',
    thumbnail: 'italsystem.jpg',
  }
];

export const CERTIFICATES: CvItem[] = [
  {
    title: 'AWS Techshift',
    subtitle: 'Amazon Web Services (AWS)',
    begin: '',
    end: '2021-04-14',
    description: '',
    link: '',
    thumbnail: '',
    attachment: '',
  },
  {
    title: 'Trinity College Certification for the English language',
    subtitle: 'LTS Language & Testing Service',
    begin: '',
    end: '2006-05-31',
    description: '',
    thumbnail: '',
    attachment: '',
  },
];

export const EDUCATION: CvItem[] = [
  {
    title: 'Università degli studi di Salerno',
    subtitle: 'Laurea in Informatica',
    begin: '2006-10',
    end: '2011-09',
    description: `Tesi: Sensore virtuale di stati ambientali per Android
    Progettazione e sviluppo del Sensore virtuale di stati ambientali per Android, libreria 
che permette l’interrogazione degli scenari ambientali in cui un dispositivo Android 
può trovarsi. La libreria è stata pubblicata con modalità LGPL su Sourceforge. Inoltre è 
stata implementata un’applicazione dimostrativa, “TelephonyManager”, che permette 
la modifica delle modalità telefoniche, disponibile su Google Play e Sourceforge. 
Relatore Prof. Vittorio Scarano.`,
    tags: ['Android', 'Subversion', 'Java', 'C', 'Assembler', 'eclipse'],
    link: 'https://www.unisa.it/',
    thumbnail: 'logo_unisa.png',
  },
];

export const LANGUAGES = [
  // RATE YOURSELF  =>  100% = NATIVE;  80-99% = FLUENT;  60-79% = ADVANCED;  40-59% = INTERMEDIATE;  20-39% = ELEMENTARY;  0-19% = BEGINNER
  { title: 'English', level: '80' },
  { title: 'Italian | Italiano', level: '100' },
  { title: 'French | Français', level: '10' },
];

export const PROJECTS: CvItem[] = [
  {
    title: 'NodeJS Backend Bootstrap',
    subtitle: 'Backend bootstrap using best technologies and frameworks for NodeJS like: Express, NestJs, Prisma',
    begin: '2021-11',
    end: '',
    description: 'This template is aimed to facilitate and speed up developers start project or, at least, study the frameworks and how to use them',
    tags: ['NodeJs', 'Graphql', 'Prisma', 'TypeScript', 'VisualStudio Code', 'GitLab', 'REST', 'NestJs'],
    link: 'https://gitlab.com/angelosantarella/nodejs-backend-bootstrap',
    thumbnail: 'nodejs.jpg',
  },
  {
    title: 'AngularCV',
    subtitle: 'A simple self-hosted online-CV',
    begin: '2021-11',
    end: '',
    description: 'This project was created for the purpose of having a basic online-CV, which anyone can host by themselves. ' +
      'This very website is the result of it.',
    tags: ['Angular', 'MaterialDesign', 'Apollo', 'CSS', 'TypeScript', 'VisualStudio Code', 'GitLab', 'jsPDF', 'npm'],
    link: 'https://gitlab.com/angelosantarella/angelosantarella.gitlab.io',
    thumbnail: '../AngularCV.svg',
  },
];

export const CONTACT = {
  city: 'Montella, Italy',
  phone: '',
  mail: 'angelo.santarella@gmail.com',
  skype: '', // just the account name
  linkedin: 'https://www.linkedin.com/in/angelosantarella/', // full url
  xing: '', // full url
  github: 'https://github.com/duky2003', // full url
  gitlab: 'https://gitlab.com/angelosantarella', // full url
  stackoverflow: '', // full url
  twitter: 'https://twitter.com/angelosantarella', // full url
  facebook: 'https://www.facebook.com/angelo.santarella/', // full url
  instagram: 'https://www.instagram.com/angelosantarella/', // full url
  other: [
    { title: 'GitLab Page', icon: 'icon-gitlab', link: 'https://angelosantarella.gitlab.io' },
    { title: 'YouTube', icon: 'icon-youtube', link: 'https://www.youtube.com/c/AngeloSantarella' },
  ],
};

export const INTERESTS = [
  {
    title: 'Ciclismo',
    icon: 'directions_bike',
  },
  {
    title: 'Software Open Source',
    icon: 'code',
  },
  {
    title: 'Droni',
    icon: 'flight',
  },
  {
    title: 'Drone video',
    icon: '4k',
  },
  {
    title: 'Photography',
    icon: 'camera_alt',
  },
  {
    title: '3d Printing',
    icon: 'view_in_ar',
  },
];

